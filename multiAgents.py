# multiAgents.py
# --------------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
# 
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).
import numpy as np

from util import manhattanDistance
from game import Directions
import random, util

from game import Agent


class ReflexAgent(Agent):
    """
    A reflex agent chooses an action at each choice point by examining
    its alternatives via a state evaluation function.

    The code below is provided as a guide.  You are welcome to change
    it in any way you see fit, so long as you don't touch our method
    headers.
    """

    def getAction(self, gameState):
        """
        You do not need to change this method, but you're welcome to.

        getAction chooses among the best options according to the evaluation function.

        Just like in the previous project, getAction takes a GameState and returns
        some Directions.X for some X in the set {NORTH, SOUTH, WEST, EAST, STOP}
        """
        # Collect legal moves and successor states
        legalMoves = gameState.getLegalActions()

        # Choose one of the best actions
        scores = [self.evaluationFunction(gameState, action) for action in legalMoves]
        bestScore = max(scores)
        bestIndices = [index for index in range(len(scores)) if scores[index] == bestScore]
        chosenIndex = random.choice(bestIndices)  # Pick randomly among the best

        "Add more of your code here if you want to"

        return legalMoves[chosenIndex]

    def evaluationFunction(self, currentGameState, action):
        """
        Design a better evaluation function here.

        The evaluation function takes in the current and proposed successor
        GameStates (pacman.py) and returns a number, where higher numbers are better.

        The code below extracts some useful information from the state, like the
        remaining food (newFood) and Pacman position after moving (newPos).
        newScaredTimes holds the number of moves that each ghost will remain
        scared because of Pacman having eaten a power pellet.

        Print out these variables to see what you're getting, then combine them
        to create a masterful evaluation function.
        """
        # Useful information you can extract from a GameState (pacman.py)
        successorGameState = currentGameState.generatePacmanSuccessor(action)
        newPos = successorGameState.getPacmanPosition()
        newFood = successorGameState.getFood()
        newGhostStates = successorGameState.getGhostStates()
        newScaredTimes = [ghostState.scaredTimer for ghostState in newGhostStates]

        "*** YOUR CODE HERE ***"
        return successorGameState.getScore()


def scoreEvaluationFunction(currentGameState):
    """
    This default evaluation function just returns the score of the state.
    The score is the same one displayed in the Pacman GUI.

    This evaluation function is meant for use with adversarial search agents
    (not reflex agents).
    """
    return currentGameState.getScore()


class MultiAgentSearchAgent(Agent):
    """
    This class provides some common elements to all of your
    multi-agent searchers.  Any methods defined here will be available
    to the MinimaxPacmanAgent, AlphaBetaPacmanAgent & ExpectimaxPacmanAgent.

    You *do not* need to make any changes here, but you can if you want to
    add functionality to all your adversarial search agents.  Please do not
    remove anything, however.

    Note: this is an abstract class: one that should not be instantiated.  It's
    only partially specified, and designed to be extended.  Agent (game.py)
    is another abstract class.
    """

    def __init__(self, evalFn='scoreEvaluationFunction', depth='2'):
        self.index = 0  # Pacman is always agent index 0
        self.evaluationFunction = util.lookup(evalFn, globals())
        self.depth = int(depth)


class MinimaxAgent(MultiAgentSearchAgent):
    """
    Your minimax agent (question 2)
    """

    def getAction(self, gameState):
        """
        Returns the minimax action from the current gameState using self.depth
        and self.evaluationFunction.

        Here are some method calls that might be useful when implementing minimax.

        gameState.getLegalActions(agentIndex):
        Returns a list of legal actions for an agent
        agentIndex=0 means Pacman, ghosts are >= 1

        gameState.generateSuccessor(agentIndex, action):
        Returns the successor game state after an agent takes an action

        gameState.getNumAgents():
        Returns the total number of agents in the game

        gameState.isWin():
        Returns whether or not the game state is a winning state

        gameState.isLose():
        Returns whether or not the game state is a losing state
        """
        num_agents = gameState.getNumAgents()  # Total number of agents

        # PACMAN function
        def max(gameState, ply):
            if gameState.isWin() or gameState.isLose():
                return gameState.getScore()  # Game is finished, return score

            best_action = Directions.STOP  # Defaults to STOP in case no better move is found
            best_score = -np.inf
            score = -np.inf

            actions = gameState.getLegalActions(0)  # Retrieves all possible pacman actions for the current state
            for a in actions:
                successor = gameState.generateSuccessor(0, a)  # Generates a successor state following the move
                score = min(successor, ply, 1)  # After finishing pacman move, minimize is run from ghost # 1

                if score > best_score:  # Is the current subtree of actions the best? Save score and move
                    best_score = score
                    best_action = a

            return best_action if ply == 0 else best_score  # If depth is zero the minmax is finished and move decided

        # Ghost function (they move randomly)
        def min(gameState, ply, ghost):
            if gameState.isWin() or gameState.isLose():
                return gameState.getScore()  # Game is finished, return score to max

            best_score = np.inf
            score = np.inf

            next_agent = ghost + 1 if ghost != (num_agents - 1) else 0      # Iterates the next agent

            actions = gameState.getLegalActions(ghost)  # Retrieves all possible ghost actions for the current state
            for a in actions:
                successor = gameState.generateSuccessor(ghost, a) # Generates a successor state following the move
                if next_agent == 0:  # If pacmans turn
                    if ply == self.depth - 1:  # Is the max depth reached? Saving state score
                        score = self.evaluationFunction(successor)
                    else:
                        score = max(successor, ply + 1)  # Not on max depth yet, carries on to next depth or "state"
                else:
                    score = min(successor, ply, next_agent)  # Next ghost turn using state generated by current ghost

                if score < best_score:
                    best_score = score
            return best_score

        return max(gameState, 0)


class AlphaBetaAgent(MultiAgentSearchAgent):
    """
    Your minimax agent with alpha-beta pruning (question 3)
    """

    def getAction(self, gameState):
        """
        Returns the minimax action using self.depth and self.evaluationFunction
        """
        num_agents = gameState.getNumAgents()

        # PACMAN function
        def maximize(gameState, ply, alpha, beta):
            if gameState.isWin() or gameState.isLose():
                return gameState.getScore()  # Game finished, return score

            best_action = Directions.STOP  # Defaults to STOP in case no better move is found
            best_score = -np.inf
            score = -np.inf

            actions = gameState.getLegalActions(0)  # Retrieves all possible actions in this state for pacman
            for a in actions:
                successor = gameState.generateSuccessor(0, a)  # Generates all possible moves
                score = minimize(successor, ply, 1, alpha, beta)  # Lets minimize function find the minimal state

                if score > best_score:
                    best_score = score
                    best_action = a
                    alpha = max(alpha, best_score)  # The best score is saved as alpha

                if best_score > beta:  # If best_score beat mins beta, then save for pruning
                    return best_score

            return best_action if ply == 0 else best_score # If depth is zero the minmax is finished and move decided

        # Ghost function (they move randomly)

        def minimize(gameState, ply, ghost, alpha, beta):

            if gameState.isWin() or gameState.isLose(): # Game is finished, return score
                return gameState.getScore()

            best_score = np.inf
            score = np.inf
            next_agent = ghost + 1 if ghost != (num_agents - 1) else 0   # Iterates the agents

            actions = gameState.getLegalActions(ghost)  # Return all possible moves in this state for current ghost
            for a in actions:
                successor = gameState.generateSuccessor(ghost, a)  # Generates successor states using the current move
                if next_agent == 0:  # pacmans turn
                    if ply == self.depth - 1:  # Is the bottom depth level reached?
                        score = self.evaluationFunction(successor)  # Reached max depth, saving state score
                    else:
                        score = maximize(successor, ply + 1, alpha, beta)  # Not on max depth yet, carries on
                else:
                    score = minimize(successor, ply, next_agent, alpha, beta)  # Next ghost

                if score < best_score:
                    best_score = score
                    beta = min(beta, best_score)  # Least score is saved as beta

                if best_score < alpha:   # If the best score is already less than alpha, return,
                    return best_score
            return best_score

        return maximize(gameState, 0, -np.inf, np.inf)


class ExpectimaxAgent(MultiAgentSearchAgent):
    """
      Your expectimax agent (question 4)
    """

    def getAction(self, gameState):
        """
        Returns the expectimax action using self.depth and self.evaluationFunction

        All ghosts should be modeled as choosing uniformly at random from their
        legal moves.
        """
        "*** YOUR CODE HERE ***"
        util.raiseNotDefined()


def betterEvaluationFunction(currentGameState):
    """
    Your extreme ghost-hunting, pellet-nabbing, food-gobbling, unstoppable
    evaluation function (question 5).

    DESCRIPTION: <write something here so we know what you did>
    """
    "*** YOUR CODE HERE ***"
    util.raiseNotDefined()


# Abbreviation
better = betterEvaluationFunction
